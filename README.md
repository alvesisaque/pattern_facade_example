# Design Pattern Façade

Projeto destinado à disciplina de Projeto destinado à disciplina MAC413/5714 - Tópicos Avançados de Programação Orientada a Objetos (2021) IME/USP. que propoe a implementação de exemplo do padrão estrutural Façade, que propoe encapsular um subsistema complexo dentro de um único objeto de interface. 

O exemplo à seguir apresenta a implementação de um sistema identificador de tópicos cientificos utilizando processamento de linguagem natural, onde o arquivo SITHAPI.py faz o papel de Façade do padrão.

## Executando o projeto
Primeiro, para garantir que as dependencias e versoes usadas aqui não interfiram com o ambiente do seu computador, indico utilizar o virtualenv. Logo após ter o virtualenv instalado, execute:

```
mkvirtualenv -p python3 pattern_facade_example
```
Em seguida instale as dependencias do projeto com 
```
pip3 install -r requirements.txt
```
```
python3 -m spacy download en_core_web_sm
```

Em seguida, importe e utilize as funções apresentadas no SITHAPI.py, como no exemplo à seguir:

```
from SITHAPI import SITHAPI
api = SITHAPI()
api.run_all()
```

Criei também um arquivo que auxilia a execução do exemplo, esse exemplo le e divide os dados para teste e treinamento. para executar basta acessar a pasta **src/** e execute:

```
python example.py
```

## Visão da Arquitetura

### Representação das Relações

[<img src="img/facade.png" width="1300" title="">]()

O diagrama apresenta cada etapa que será seguido para que o projeto funcione. Relacionando e organizando as relações. É possível notar a classe SITHAPI.py em azul é a interface que cria uma fachada de interação com o sistema.
### Diagrama de Pacotes
[<img src="img/diagrama_pacotes.png" width="1300" title="">]()