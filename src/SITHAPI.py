from Splitter import Splitter
from DataLoader import DataLoader
from Cleaner import Cleaner
from PreProcessor import PreProcessor
from Tfidf import Tfidf
import pandas as pd
from utils import remove_special_characters, clean_broke_lines, default_data_filename


class SITHAPI:
    def __init__(self):
        self._dataLoader = DataLoader(default_data_filename)
        self._cleaner = Cleaner(dataframe=pd.DataFrame())
        self._splitter = Splitter(dataframe=pd.DataFrame())
        self._preProcessor = PreProcessor(texts = pd.Series(), list_of_tokens = pd.Series())
        self._tfidf = Tfidf(list_of_tokens = pd.Series())

    def run_all(self):        
        dataframe = self._dataLoader.get_dataframe()
        dataframe = self._cleaner.clean_dataset(dataframe)

        self._splitter.data_split(dataframe)
        texts = dataframe.text
        list_of_tokens = self._preProcessor.tokenize_multiple_texts(texts, include_stopwords=False, include_punctuation=False,
                                                                    include_like_num=False, include_only_ascii=True, include_pos=[])

        list_of_tokens = self._preProcessor.lemmatize_multiple_texts(list_of_tokens)
        list_of_tokens = self._preProcessor.lowercase_list_of_tokes(list_of_tokens)

        vectors_list, feature_names = self._tfidf.get_tf_idf(
            list_of_tokens, ngram_min=1, ngram_max=1)

        return self._tfidf.get_most_significant_tokens(vectors_list, feature_names, number_of_tokens=10)
    
    def get_dataframe(self):
        return self._dataLoader.get_dataframe()

    def cleaning(self, default_data_filename):
        dataframe = self._dataLoader.get_dataframe(default_data_filename)
        dataframe = self._cleaner.clean_dataset(dataframe)
        return dataframe

    def split(self, dataframe):
        self._splitter.data_split(dataframe)

    def tokenize_single_text(self, text, include_stopwords=False, include_punctuation=False,
                             include_like_num=False, include_only_ascii=True, include_pos=[]):
        return self._preProcessor.tokenize_single_text(text, include_stopwords, include_punctuation,
                                                       include_like_num, include_only_ascii, include_pos=[])

    def tokenize_multiple_texts(self, texts, include_stopwords=False, include_punctuation=False,
                                include_like_num=False, include_only_ascii=True, include_pos=[]):
        return self._preProcessor.tokenize_multiple_texts(texts, include_stopwords, include_punctuation,
                                                          include_like_num, include_only_ascii, include_pos=[])

    def get_tf_idf(self, list_of_tokens, ngram_min=1, ngram_max=1):
        return self._tfidf.get_tf_idf(list_of_tokens, ngram_min, ngram_max)

    def get_tf_idf_matrix(self, vectors_list, feature_names):
        return self._tfidf.get_tf_idf_matrix(vectors_list, feature_names)

    def get_most_significant_tokens(self, vectors_list, feature_names, number_of_tokens=10):
        return self._tfidf.get_most_significant_tokens(vectors_list, feature_names, number_of_tokens)
